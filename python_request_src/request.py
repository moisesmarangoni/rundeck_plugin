#!/usr/bin/python
import sys
import urllib

print 'lista de argumentos:', str(sys.argv)

#Cria string com os valores dos argumentos
comando_arg = ""
for x in range(2, len(sys.argv)-1):
	comando_arg += " " + str(sys.argv[x])
time_out = sys.argv[len(sys.argv)-1]
hostname = sys.argv[1]
send_arg_cmd = "::" + comando_arg + "::" + time_out
print "argumentos passados: " + comando_arg + " timeout " + str(time_out) + " hostname: " + hostname
#params = {'comando': '::dir /w ::2000::'}
params = {'comando': send_arg_cmd}
query = urllib.urlencode(params)
url = "http://"+ hostname +":8080/"
print "query montada: " + query
f = urllib.urlopen(url, urllib.unquote_plus(query))
contents = f.read()
f.close()
#str1 = "This be a string"
saida_erro = ":1::"
if saida_erro in contents:
    #print saida_erro, " is been found in ", comando_arg
	print contents
	exit(1)
else:
    #print saida_erro, " is not found in ", comando_arg
	print contents
	exit(0)
#print contents
